<img src="https://gitlab.com/lf.aguiar/zuppay/-/raw/master/logo_zuppay.png" />

ZUPPAY
=====

A startup ZUPPAY está convidando você a se juntar a um squad para construir um novo aplicativo de processamento de pagamentos! Como arquiteto da solução, você é o responsável por projetar e implementar o processo de autorização de um pagamento solicitado por um usuário da plataforma.

Você pode usar qualquer linguagem de programação que suporte protocolo HTTP ou GRPC.

## Instruções:

##### Requisitos:
- Um diagrama de projeto da solução
- O aplicativo deve usar as melhores práticas - de conformidade PCI
- O pedido de pagamento deve ser idempotente
- O processo de pagamento deve ser assíncrono
- O status do pagamento deve ser notificado de alguma forma ao usuário

##### Campos obrigatórios na app:
> description: String (100)

> amount: Int (in cents)

> currency: String (supports only USD, EUR)

> creditCard: Object (card fields)

## O que iremos avaliar:
- Aderência aos requisitos
- Elegância da solução
- Documentação
- Defesa da solução por meio de apresentação
- O código-fonte deve estar em um repositório Git
- Ele deve conter um arquivo chamado README.md descrevendo a solução e como testar e executar o projeto
